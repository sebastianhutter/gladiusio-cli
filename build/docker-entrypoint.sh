#!/bin/sh
set -e

# docker entrypoint
# replace hardcoded configuration values in cli config files
# exec given parameters

# 2018-04-01: inspired by https://gist.github.com/pstadler/779ca2a4330c189b4c5b174469876cbc
# for the time being the cli does not support configuration via environment variables or parameters
# to make the cli work with remote nodes we need to replace the host configuration in the file
# - config.js (config.js:5:  controlDaemonAddress: "http://localhost", // What address to access the daemon) 
# - index.js (index.js:47:  host: 'localhost')

sed 's/\(^\s*controlDaemonAddress: "http:\/\/\)localhost"/\1"+process.env.ADDRESS/g' "/config.js.orig" > /npm/lib/node_modules/gladius-cli/config.js
sed 's/\(^\s*host:\) .localhost./\1 process.env.ADDRESS/g' "/index.js.orig" > /npm/lib/node_modules/gladius-cli/index.js

exec $@