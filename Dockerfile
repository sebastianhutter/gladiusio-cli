FROM node:9-alpine

# setup npm directory
RUN mkdir /npm \
 && chown node:node /npm

# set the default address to localhost
ENV ADDRESS=localhost

# become user node and setup installation directory
USER node
ENV PATH=/npm/bin:$PATH
ENV NPM_CONFIG_PREFIX=/npm

# install the gladius cli
RUN npm install -g gladius-cli

# declare gladius-cli as volume
# TODO: split configuration files to separate volume
VOLUME /npm/lib/node_modules/gladius-cli

# add entrypoint script
USER root
# setup entrypoint
ADD build/docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
# create a copy of the original configuration files
# values in the config files will be replaced during start of container
RUN cd /npm/lib/node_modules/gladius-cli \
  && cp config.js "/config.js.orig" \
  && cp index.js "/index.js.orig" \
  && chown node:node "/config.js.orig" "/index.js.orig"
USER node

# set the entrypoint and display help if executed
# without any parameters
ENTRYPOINT [ "/docker-entrypoint.sh" ]
CMD [ "gladius-node", "--help" ]