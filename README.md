# DEPRECATED
DO NOT USE

# gladiusio-cli
Docker image to run the gladius cli: https://github.com/gladiusio/gladius-cli

## Usage
The goal behind this image is to split the cli away from the edge and control daemon.
This allows to store the cli data away from nodes without any local disks (my infrastructure is running on nodes without persistent disks).

For example: I have the control daemon and the edge daemon running on a Linux machine  but I execute
the cli from my macbook.

The image exports the volume `/npm/lib/node_modules/gladius-cli` to keep the generated keys and nodeFile.json 'safe'.
With the environment variable `ADDRESS` you can change the host for the `gladius-node` command.

*Example*:
:
```
# run init on the remote linux machine with the ip 192.168.1.201 
# store the generated cli config locally in /Users/sebastian/gladius
docker run --rm -ti \
  -v /Users/sebastian/gladius:/npm/lib/node_modules/gladius-cli \
  -e ADDRESS=192.168.1.201 \
  registry.gitlab.com/sebastianhutter/gladiusio-cli \
  gladius-node init
```

## edge and control daemon
The edge and control daemon images can be found in the corresponding gitlab repositories:
- https://gitlab.com/sebastianhutter/gladiusio-edge-daemon
- https://gitlab.com/sebastianhutter/gladiusio-control-daemon

### docker-compose
Simplest way to start both containers on a node is to use docker-compose:
```
# docker-compose.yml for gladius control and edge daemon
version: '3'
services:
  gladius-control:
    image: registry.gitlab.com/sebastianhutter/gladiusio-control-daemon:latest
    tty: true
    ports:
      - 3000:3000/tcp
  gladius-edge:
    image: registry.gitlab.com/sebastianhutter/gladiusio-edge-daemon:latest
    tty: true
    ports:
      - 5000:5000/tcp
      - 8080:8080/tcp

```